# androidone-2nd-tool

### Araç ana menü
![Image of tool](https://gitlab.com/doopthe/androidone-2nd-tool/raw/master/mainmenu.png)

### Neler yapabiliyor ?
* Son sürüm twrp indirip kurabilme (Crackling, Seed)
* Oem lock,Unlock
* Sürücüleri Yükleme (Şimdilik: Arch, Debian ve ubuntu)
* Otomatik root
* Güncel Magisk indirme ve kurma

##  Kurulum

* ```git clone https://github.com/TheDoop/androidone-2nd-tool.git```
* ```cd androidone-2nd-tool```
* ```chmod +x tool```
* ```./tool```

### Destek
* Telegram
[Telegram](http://t.me/F_Doop)
* Telegram Seed,Crackling Grubu
[Telegram-Group](https://t.me/joinchat/CWef7kZYepljmBOwTdnmcQ)
